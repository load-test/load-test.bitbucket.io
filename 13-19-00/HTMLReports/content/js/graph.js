/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
$(document).ready(function() {

    $(".click-title").mouseenter( function(    e){
        e.preventDefault();
        this.style.cursor="pointer";
    });
    $(".click-title").mousedown( function(event){
        event.preventDefault();
    });

    // Ugly code while this script is shared among several pages
    try{
        refreshHitsPerSecond(true);
    } catch(e){}
    try{
        refreshResponseTimeOverTime(true);
    } catch(e){}
    try{
        refreshResponseTimePercentiles();
    } catch(e){}
});


var responseTimePercentilesInfos = {
        data: {"result": {"minY": 14.0, "minX": 0.0, "maxY": 5140.0, "series": [{"data": [[0.0, 14.0], [0.1, 15.0], [0.2, 16.0], [0.3, 16.0], [0.4, 16.0], [0.5, 16.0], [0.6, 17.0], [0.7, 17.0], [0.8, 17.0], [0.9, 17.0], [1.0, 17.0], [1.1, 17.0], [1.2, 17.0], [1.3, 17.0], [1.4, 18.0], [1.5, 18.0], [1.6, 18.0], [1.7, 18.0], [1.8, 18.0], [1.9, 18.0], [2.0, 18.0], [2.1, 18.0], [2.2, 18.0], [2.3, 19.0], [2.4, 19.0], [2.5, 19.0], [2.6, 19.0], [2.7, 19.0], [2.8, 20.0], [2.9, 20.0], [3.0, 20.0], [3.1, 21.0], [3.2, 21.0], [3.3, 21.0], [3.4, 22.0], [3.5, 22.0], [3.6, 23.0], [3.7, 24.0], [3.8, 25.0], [3.9, 26.0], [4.0, 28.0], [4.1, 30.0], [4.2, 34.0], [4.3, 41.0], [4.4, 56.0], [4.5, 68.0], [4.6, 75.0], [4.7, 79.0], [4.8, 83.0], [4.9, 85.0], [5.0, 87.0], [5.1, 89.0], [5.2, 91.0], [5.3, 93.0], [5.4, 96.0], [5.5, 98.0], [5.6, 100.0], [5.7, 100.0], [5.8, 102.0], [5.9, 103.0], [6.0, 104.0], [6.1, 105.0], [6.2, 106.0], [6.3, 107.0], [6.4, 107.0], [6.5, 108.0], [6.6, 109.0], [6.7, 109.0], [6.8, 110.0], [6.9, 111.0], [7.0, 112.0], [7.1, 112.0], [7.2, 113.0], [7.3, 114.0], [7.4, 115.0], [7.5, 115.0], [7.6, 116.0], [7.7, 117.0], [7.8, 117.0], [7.9, 118.0], [8.0, 118.0], [8.1, 119.0], [8.2, 119.0], [8.3, 120.0], [8.4, 120.0], [8.5, 121.0], [8.6, 121.0], [8.7, 122.0], [8.8, 122.0], [8.9, 123.0], [9.0, 123.0], [9.1, 124.0], [9.2, 124.0], [9.3, 125.0], [9.4, 125.0], [9.5, 126.0], [9.6, 126.0], [9.7, 127.0], [9.8, 127.0], [9.9, 128.0], [10.0, 128.0], [10.1, 129.0], [10.2, 129.0], [10.3, 130.0], [10.4, 130.0], [10.5, 131.0], [10.6, 131.0], [10.7, 132.0], [10.8, 132.0], [10.9, 133.0], [11.0, 133.0], [11.1, 134.0], [11.2, 134.0], [11.3, 135.0], [11.4, 135.0], [11.5, 135.0], [11.6, 136.0], [11.7, 136.0], [11.8, 136.0], [11.9, 137.0], [12.0, 137.0], [12.1, 138.0], [12.2, 138.0], [12.3, 139.0], [12.4, 140.0], [12.5, 140.0], [12.6, 140.0], [12.7, 141.0], [12.8, 141.0], [12.9, 142.0], [13.0, 142.0], [13.1, 142.0], [13.2, 143.0], [13.3, 143.0], [13.4, 143.0], [13.5, 144.0], [13.6, 144.0], [13.7, 144.0], [13.8, 145.0], [13.9, 145.0], [14.0, 146.0], [14.1, 146.0], [14.2, 146.0], [14.3, 147.0], [14.4, 147.0], [14.5, 147.0], [14.6, 148.0], [14.7, 148.0], [14.8, 148.0], [14.9, 149.0], [15.0, 149.0], [15.1, 149.0], [15.2, 150.0], [15.3, 150.0], [15.4, 150.0], [15.5, 150.0], [15.6, 151.0], [15.7, 151.0], [15.8, 151.0], [15.9, 152.0], [16.0, 152.0], [16.1, 152.0], [16.2, 153.0], [16.3, 153.0], [16.4, 154.0], [16.5, 154.0], [16.6, 154.0], [16.7, 155.0], [16.8, 155.0], [16.9, 155.0], [17.0, 156.0], [17.1, 156.0], [17.2, 156.0], [17.3, 156.0], [17.4, 157.0], [17.5, 157.0], [17.6, 157.0], [17.7, 158.0], [17.8, 158.0], [17.9, 158.0], [18.0, 159.0], [18.1, 159.0], [18.2, 159.0], [18.3, 160.0], [18.4, 160.0], [18.5, 160.0], [18.6, 161.0], [18.7, 161.0], [18.8, 161.0], [18.9, 161.0], [19.0, 162.0], [19.1, 162.0], [19.2, 162.0], [19.3, 162.0], [19.4, 162.0], [19.5, 163.0], [19.6, 163.0], [19.7, 163.0], [19.8, 164.0], [19.9, 164.0], [20.0, 164.0], [20.1, 164.0], [20.2, 165.0], [20.3, 165.0], [20.4, 165.0], [20.5, 165.0], [20.6, 165.0], [20.7, 166.0], [20.8, 166.0], [20.9, 166.0], [21.0, 166.0], [21.1, 167.0], [21.2, 167.0], [21.3, 167.0], [21.4, 167.0], [21.5, 167.0], [21.6, 168.0], [21.7, 168.0], [21.8, 168.0], [21.9, 168.0], [22.0, 169.0], [22.1, 169.0], [22.2, 169.0], [22.3, 169.0], [22.4, 169.0], [22.5, 170.0], [22.6, 170.0], [22.7, 170.0], [22.8, 170.0], [22.9, 170.0], [23.0, 171.0], [23.1, 171.0], [23.2, 171.0], [23.3, 171.0], [23.4, 171.0], [23.5, 172.0], [23.6, 172.0], [23.7, 172.0], [23.8, 173.0], [23.9, 173.0], [24.0, 173.0], [24.1, 173.0], [24.2, 173.0], [24.3, 174.0], [24.4, 174.0], [24.5, 174.0], [24.6, 174.0], [24.7, 174.0], [24.8, 174.0], [24.9, 175.0], [25.0, 175.0], [25.1, 175.0], [25.2, 175.0], [25.3, 175.0], [25.4, 175.0], [25.5, 176.0], [25.6, 176.0], [25.7, 176.0], [25.8, 176.0], [25.9, 176.0], [26.0, 177.0], [26.1, 177.0], [26.2, 177.0], [26.3, 177.0], [26.4, 177.0], [26.5, 177.0], [26.6, 178.0], [26.7, 178.0], [26.8, 178.0], [26.9, 178.0], [27.0, 178.0], [27.1, 179.0], [27.2, 179.0], [27.3, 179.0], [27.4, 179.0], [27.5, 179.0], [27.6, 180.0], [27.7, 180.0], [27.8, 180.0], [27.9, 180.0], [28.0, 180.0], [28.1, 181.0], [28.2, 181.0], [28.3, 181.0], [28.4, 181.0], [28.5, 181.0], [28.6, 181.0], [28.7, 182.0], [28.8, 182.0], [28.9, 182.0], [29.0, 182.0], [29.1, 182.0], [29.2, 182.0], [29.3, 183.0], [29.4, 183.0], [29.5, 183.0], [29.6, 183.0], [29.7, 183.0], [29.8, 183.0], [29.9, 184.0], [30.0, 184.0], [30.1, 184.0], [30.2, 184.0], [30.3, 184.0], [30.4, 184.0], [30.5, 185.0], [30.6, 185.0], [30.7, 185.0], [30.8, 185.0], [30.9, 185.0], [31.0, 185.0], [31.1, 186.0], [31.2, 186.0], [31.3, 186.0], [31.4, 186.0], [31.5, 186.0], [31.6, 186.0], [31.7, 187.0], [31.8, 187.0], [31.9, 187.0], [32.0, 187.0], [32.1, 187.0], [32.2, 187.0], [32.3, 188.0], [32.4, 188.0], [32.5, 188.0], [32.6, 188.0], [32.7, 188.0], [32.8, 189.0], [32.9, 189.0], [33.0, 189.0], [33.1, 189.0], [33.2, 189.0], [33.3, 189.0], [33.4, 189.0], [33.5, 189.0], [33.6, 190.0], [33.7, 190.0], [33.8, 190.0], [33.9, 190.0], [34.0, 190.0], [34.1, 190.0], [34.2, 191.0], [34.3, 191.0], [34.4, 191.0], [34.5, 191.0], [34.6, 191.0], [34.7, 191.0], [34.8, 192.0], [34.9, 192.0], [35.0, 192.0], [35.1, 192.0], [35.2, 192.0], [35.3, 192.0], [35.4, 192.0], [35.5, 193.0], [35.6, 193.0], [35.7, 193.0], [35.8, 193.0], [35.9, 193.0], [36.0, 193.0], [36.1, 193.0], [36.2, 194.0], [36.3, 194.0], [36.4, 194.0], [36.5, 194.0], [36.6, 194.0], [36.7, 194.0], [36.8, 194.0], [36.9, 195.0], [37.0, 195.0], [37.1, 195.0], [37.2, 195.0], [37.3, 195.0], [37.4, 195.0], [37.5, 195.0], [37.6, 196.0], [37.7, 196.0], [37.8, 196.0], [37.9, 196.0], [38.0, 196.0], [38.1, 196.0], [38.2, 197.0], [38.3, 197.0], [38.4, 197.0], [38.5, 197.0], [38.6, 197.0], [38.7, 197.0], [38.8, 197.0], [38.9, 198.0], [39.0, 198.0], [39.1, 198.0], [39.2, 198.0], [39.3, 199.0], [39.4, 199.0], [39.5, 199.0], [39.6, 199.0], [39.7, 199.0], [39.8, 199.0], [39.9, 200.0], [40.0, 200.0], [40.1, 200.0], [40.2, 200.0], [40.3, 200.0], [40.4, 200.0], [40.5, 201.0], [40.6, 201.0], [40.7, 201.0], [40.8, 201.0], [40.9, 201.0], [41.0, 202.0], [41.1, 202.0], [41.2, 202.0], [41.3, 202.0], [41.4, 202.0], [41.5, 202.0], [41.6, 203.0], [41.7, 203.0], [41.8, 203.0], [41.9, 203.0], [42.0, 203.0], [42.1, 203.0], [42.2, 204.0], [42.3, 204.0], [42.4, 204.0], [42.5, 204.0], [42.6, 204.0], [42.7, 205.0], [42.8, 205.0], [42.9, 205.0], [43.0, 205.0], [43.1, 205.0], [43.2, 205.0], [43.3, 206.0], [43.4, 206.0], [43.5, 206.0], [43.6, 206.0], [43.7, 206.0], [43.8, 206.0], [43.9, 207.0], [44.0, 207.0], [44.1, 207.0], [44.2, 207.0], [44.3, 207.0], [44.4, 208.0], [44.5, 208.0], [44.6, 208.0], [44.7, 208.0], [44.8, 208.0], [44.9, 209.0], [45.0, 209.0], [45.1, 209.0], [45.2, 209.0], [45.3, 210.0], [45.4, 210.0], [45.5, 210.0], [45.6, 210.0], [45.7, 210.0], [45.8, 211.0], [45.9, 211.0], [46.0, 211.0], [46.1, 211.0], [46.2, 211.0], [46.3, 212.0], [46.4, 212.0], [46.5, 212.0], [46.6, 212.0], [46.7, 212.0], [46.8, 213.0], [46.9, 213.0], [47.0, 213.0], [47.1, 213.0], [47.2, 214.0], [47.3, 214.0], [47.4, 214.0], [47.5, 214.0], [47.6, 215.0], [47.7, 215.0], [47.8, 215.0], [47.9, 215.0], [48.0, 216.0], [48.1, 216.0], [48.2, 216.0], [48.3, 217.0], [48.4, 217.0], [48.5, 217.0], [48.6, 217.0], [48.7, 218.0], [48.8, 218.0], [48.9, 218.0], [49.0, 219.0], [49.1, 219.0], [49.2, 219.0], [49.3, 219.0], [49.4, 220.0], [49.5, 220.0], [49.6, 220.0], [49.7, 221.0], [49.8, 221.0], [49.9, 221.0], [50.0, 222.0], [50.1, 222.0], [50.2, 222.0], [50.3, 223.0], [50.4, 223.0], [50.5, 223.0], [50.6, 223.0], [50.7, 224.0], [50.8, 224.0], [50.9, 224.0], [51.0, 224.0], [51.1, 225.0], [51.2, 225.0], [51.3, 225.0], [51.4, 226.0], [51.5, 226.0], [51.6, 226.0], [51.7, 227.0], [51.8, 227.0], [51.9, 227.0], [52.0, 228.0], [52.1, 228.0], [52.2, 228.0], [52.3, 229.0], [52.4, 229.0], [52.5, 229.0], [52.6, 230.0], [52.7, 230.0], [52.8, 230.0], [52.9, 230.0], [53.0, 231.0], [53.1, 231.0], [53.2, 231.0], [53.3, 232.0], [53.4, 232.0], [53.5, 232.0], [53.6, 233.0], [53.7, 233.0], [53.8, 234.0], [53.9, 234.0], [54.0, 234.0], [54.1, 235.0], [54.2, 235.0], [54.3, 236.0], [54.4, 236.0], [54.5, 236.0], [54.6, 237.0], [54.7, 237.0], [54.8, 238.0], [54.9, 239.0], [55.0, 239.0], [55.1, 239.0], [55.2, 240.0], [55.3, 240.0], [55.4, 241.0], [55.5, 241.0], [55.6, 241.0], [55.7, 242.0], [55.8, 242.0], [55.9, 243.0], [56.0, 243.0], [56.1, 243.0], [56.2, 244.0], [56.3, 244.0], [56.4, 245.0], [56.5, 245.0], [56.6, 246.0], [56.7, 246.0], [56.8, 246.0], [56.9, 247.0], [57.0, 248.0], [57.1, 248.0], [57.2, 248.0], [57.3, 249.0], [57.4, 249.0], [57.5, 249.0], [57.6, 250.0], [57.7, 250.0], [57.8, 251.0], [57.9, 251.0], [58.0, 251.0], [58.1, 252.0], [58.2, 252.0], [58.3, 253.0], [58.4, 253.0], [58.5, 254.0], [58.6, 254.0], [58.7, 255.0], [58.8, 255.0], [58.9, 256.0], [59.0, 256.0], [59.1, 257.0], [59.2, 257.0], [59.3, 258.0], [59.4, 258.0], [59.5, 259.0], [59.6, 259.0], [59.7, 260.0], [59.8, 260.0], [59.9, 261.0], [60.0, 261.0], [60.1, 261.0], [60.2, 262.0], [60.3, 263.0], [60.4, 263.0], [60.5, 264.0], [60.6, 264.0], [60.7, 265.0], [60.8, 265.0], [60.9, 266.0], [61.0, 266.0], [61.1, 267.0], [61.2, 268.0], [61.3, 268.0], [61.4, 268.0], [61.5, 269.0], [61.6, 269.0], [61.7, 270.0], [61.8, 270.0], [61.9, 271.0], [62.0, 272.0], [62.1, 272.0], [62.2, 273.0], [62.3, 274.0], [62.4, 274.0], [62.5, 274.0], [62.6, 275.0], [62.7, 275.0], [62.8, 276.0], [62.9, 277.0], [63.0, 277.0], [63.1, 278.0], [63.2, 278.0], [63.3, 279.0], [63.4, 280.0], [63.5, 280.0], [63.6, 281.0], [63.7, 281.0], [63.8, 282.0], [63.9, 282.0], [64.0, 283.0], [64.1, 283.0], [64.2, 284.0], [64.3, 284.0], [64.4, 285.0], [64.5, 285.0], [64.6, 286.0], [64.7, 286.0], [64.8, 287.0], [64.9, 287.0], [65.0, 288.0], [65.1, 288.0], [65.2, 289.0], [65.3, 289.0], [65.4, 290.0], [65.5, 290.0], [65.6, 291.0], [65.7, 291.0], [65.8, 292.0], [65.9, 292.0], [66.0, 293.0], [66.1, 293.0], [66.2, 294.0], [66.3, 294.0], [66.4, 295.0], [66.5, 296.0], [66.6, 296.0], [66.7, 296.0], [66.8, 297.0], [66.9, 297.0], [67.0, 298.0], [67.1, 298.0], [67.2, 299.0], [67.3, 299.0], [67.4, 300.0], [67.5, 301.0], [67.6, 301.0], [67.7, 302.0], [67.8, 302.0], [67.9, 303.0], [68.0, 303.0], [68.1, 304.0], [68.2, 304.0], [68.3, 304.0], [68.4, 305.0], [68.5, 305.0], [68.6, 306.0], [68.7, 306.0], [68.8, 306.0], [68.9, 307.0], [69.0, 307.0], [69.1, 307.0], [69.2, 308.0], [69.3, 308.0], [69.4, 309.0], [69.5, 309.0], [69.6, 310.0], [69.7, 310.0], [69.8, 311.0], [69.9, 311.0], [70.0, 312.0], [70.1, 312.0], [70.2, 312.0], [70.3, 313.0], [70.4, 313.0], [70.5, 314.0], [70.6, 314.0], [70.7, 314.0], [70.8, 315.0], [70.9, 315.0], [71.0, 316.0], [71.1, 317.0], [71.2, 317.0], [71.3, 318.0], [71.4, 318.0], [71.5, 319.0], [71.6, 319.0], [71.7, 320.0], [71.8, 320.0], [71.9, 321.0], [72.0, 321.0], [72.1, 321.0], [72.2, 322.0], [72.3, 322.0], [72.4, 323.0], [72.5, 323.0], [72.6, 324.0], [72.7, 324.0], [72.8, 325.0], [72.9, 325.0], [73.0, 325.0], [73.1, 326.0], [73.2, 326.0], [73.3, 327.0], [73.4, 327.0], [73.5, 328.0], [73.6, 328.0], [73.7, 328.0], [73.8, 329.0], [73.9, 330.0], [74.0, 330.0], [74.1, 330.0], [74.2, 331.0], [74.3, 331.0], [74.4, 332.0], [74.5, 332.0], [74.6, 333.0], [74.7, 334.0], [74.8, 334.0], [74.9, 335.0], [75.0, 335.0], [75.1, 336.0], [75.2, 337.0], [75.3, 337.0], [75.4, 338.0], [75.5, 338.0], [75.6, 339.0], [75.7, 339.0], [75.8, 340.0], [75.9, 341.0], [76.0, 341.0], [76.1, 342.0], [76.2, 342.0], [76.3, 343.0], [76.4, 343.0], [76.5, 344.0], [76.6, 344.0], [76.7, 345.0], [76.8, 345.0], [76.9, 346.0], [77.0, 347.0], [77.1, 348.0], [77.2, 349.0], [77.3, 349.0], [77.4, 350.0], [77.5, 351.0], [77.6, 351.0], [77.7, 352.0], [77.8, 352.0], [77.9, 353.0], [78.0, 353.0], [78.1, 354.0], [78.2, 355.0], [78.3, 355.0], [78.4, 356.0], [78.5, 357.0], [78.6, 357.0], [78.7, 358.0], [78.8, 358.0], [78.9, 359.0], [79.0, 360.0], [79.1, 361.0], [79.2, 361.0], [79.3, 362.0], [79.4, 363.0], [79.5, 364.0], [79.6, 364.0], [79.7, 365.0], [79.8, 366.0], [79.9, 367.0], [80.0, 367.0], [80.1, 368.0], [80.2, 370.0], [80.3, 371.0], [80.4, 371.0], [80.5, 372.0], [80.6, 373.0], [80.7, 373.0], [80.8, 375.0], [80.9, 376.0], [81.0, 377.0], [81.1, 377.0], [81.2, 378.0], [81.3, 379.0], [81.4, 380.0], [81.5, 380.0], [81.6, 381.0], [81.7, 382.0], [81.8, 383.0], [81.9, 384.0], [82.0, 385.0], [82.1, 385.0], [82.2, 387.0], [82.3, 388.0], [82.4, 389.0], [82.5, 390.0], [82.6, 391.0], [82.7, 392.0], [82.8, 393.0], [82.9, 394.0], [83.0, 395.0], [83.1, 396.0], [83.2, 396.0], [83.3, 397.0], [83.4, 398.0], [83.5, 399.0], [83.6, 400.0], [83.7, 401.0], [83.8, 402.0], [83.9, 402.0], [84.0, 403.0], [84.1, 404.0], [84.2, 405.0], [84.3, 407.0], [84.4, 408.0], [84.5, 409.0], [84.6, 410.0], [84.7, 411.0], [84.8, 412.0], [84.9, 413.0], [85.0, 414.0], [85.1, 415.0], [85.2, 416.0], [85.3, 417.0], [85.4, 418.0], [85.5, 418.0], [85.6, 420.0], [85.7, 421.0], [85.8, 422.0], [85.9, 424.0], [86.0, 425.0], [86.1, 426.0], [86.2, 427.0], [86.3, 428.0], [86.4, 430.0], [86.5, 431.0], [86.6, 433.0], [86.7, 434.0], [86.8, 435.0], [86.9, 437.0], [87.0, 438.0], [87.1, 439.0], [87.2, 440.0], [87.3, 442.0], [87.4, 443.0], [87.5, 445.0], [87.6, 446.0], [87.7, 447.0], [87.8, 449.0], [87.9, 450.0], [88.0, 451.0], [88.1, 452.0], [88.2, 453.0], [88.3, 455.0], [88.4, 457.0], [88.5, 458.0], [88.6, 460.0], [88.7, 462.0], [88.8, 463.0], [88.9, 465.0], [89.0, 466.0], [89.1, 468.0], [89.2, 469.0], [89.3, 470.0], [89.4, 472.0], [89.5, 474.0], [89.6, 477.0], [89.7, 478.0], [89.8, 480.0], [89.9, 481.0], [90.0, 482.0], [90.1, 483.0], [90.2, 485.0], [90.3, 487.0], [90.4, 488.0], [90.5, 490.0], [90.6, 491.0], [90.7, 493.0], [90.8, 494.0], [90.9, 496.0], [91.0, 498.0], [91.1, 501.0], [91.2, 503.0], [91.3, 505.0], [91.4, 507.0], [91.5, 509.0], [91.6, 512.0], [91.7, 514.0], [91.8, 516.0], [91.9, 518.0], [92.0, 521.0], [92.1, 524.0], [92.2, 527.0], [92.3, 529.0], [92.4, 532.0], [92.5, 535.0], [92.6, 537.0], [92.7, 540.0], [92.8, 542.0], [92.9, 546.0], [93.0, 549.0], [93.1, 552.0], [93.2, 554.0], [93.3, 557.0], [93.4, 560.0], [93.5, 565.0], [93.6, 569.0], [93.7, 573.0], [93.8, 576.0], [93.9, 580.0], [94.0, 583.0], [94.1, 586.0], [94.2, 588.0], [94.3, 590.0], [94.4, 594.0], [94.5, 596.0], [94.6, 600.0], [94.7, 604.0], [94.8, 607.0], [94.9, 611.0], [95.0, 615.0], [95.1, 619.0], [95.2, 622.0], [95.3, 626.0], [95.4, 628.0], [95.5, 633.0], [95.6, 636.0], [95.7, 639.0], [95.8, 644.0], [95.9, 647.0], [96.0, 652.0], [96.1, 656.0], [96.2, 660.0], [96.3, 664.0], [96.4, 668.0], [96.5, 672.0], [96.6, 678.0], [96.7, 686.0], [96.8, 692.0], [96.9, 695.0], [97.0, 701.0], [97.1, 709.0], [97.2, 718.0], [97.3, 725.0], [97.4, 736.0], [97.5, 743.0], [97.6, 753.0], [97.7, 760.0], [97.8, 769.0], [97.9, 777.0], [98.0, 785.0], [98.1, 798.0], [98.2, 812.0], [98.3, 823.0], [98.4, 835.0], [98.5, 849.0], [98.6, 875.0], [98.7, 893.0], [98.8, 916.0], [98.9, 941.0], [99.0, 962.0], [99.1, 989.0], [99.2, 1012.0], [99.3, 1045.0], [99.4, 1095.0], [99.5, 1140.0], [99.6, 1214.0], [99.7, 1299.0], [99.8, 1410.0], [99.9, 3473.0], [100.0, 5140.0]], "isOverall": false, "label": "findOndWiseFlightCombinations- Oneway - Non connections", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Response Time Percentiles"}},
        getOptions: function() {
            return {
                series: {
                    points: { show: false }
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentiles'
                },
                xaxis: {
                    tickDecimals: 1,
                    axisLabel: "Percentiles",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Percentile value in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : %x.2 percentile was %y ms"
                },
                selection: { mode: "xy" },
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentiles"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesPercentiles"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesPercentiles"), dataset, prepareOverviewOptions(options));
        }
};

/**
 * @param elementId Id of element where we display message
 */
function setEmptyGraph(elementId) {
    $(function() {
        $(elementId).text("No graph series with filter="+seriesFilter);
    });
}

// Response times percentiles
function refreshResponseTimePercentiles() {
    var infos = responseTimePercentilesInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimePercentiles");
        return;
    }
    if (isGraph($("#flotResponseTimesPercentiles"))){
        infos.createGraph();
    } else {
        var choiceContainer = $("#choicesResponseTimePercentiles");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesPercentiles", "#overviewResponseTimesPercentiles");
        $('#bodyResponseTimePercentiles .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimeDistributionInfos = {
        data: {"result": {"minY": 1.0, "minX": 0.0, "maxY": 6147.0, "series": [{"data": [[0.0, 1002.0], [600.0, 426.0], [700.0, 206.0], [800.0, 109.0], [900.0, 73.0], [1000.0, 51.0], [1100.0, 31.0], [1200.0, 21.0], [1300.0, 15.0], [1400.0, 7.0], [1500.0, 5.0], [100.0, 6147.0], [1600.0, 1.0], [1700.0, 1.0], [1800.0, 1.0], [2000.0, 1.0], [2200.0, 1.0], [2300.0, 1.0], [2600.0, 1.0], [3300.0, 1.0], [200.0, 4928.0], [3400.0, 1.0], [3800.0, 4.0], [3900.0, 7.0], [4000.0, 2.0], [4200.0, 2.0], [4300.0, 1.0], [300.0, 2912.0], [5100.0, 1.0], [400.0, 1342.0], [500.0, 638.0]], "isOverall": false, "label": "findOndWiseFlightCombinations- Oneway - Non connections", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 100, "maxX": 5100.0, "title": "Response Time Distribution"}},
        getOptions: function() {
            var granularity = this.data.result.granularity;
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    barWidth: this.data.result.granularity
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " responses for " + label + " were between " + xval + " and " + (xval + granularity) + " ms";
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimeDistribution"), prepareData(data.result.series, $("#choicesResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshResponseTimeDistribution() {
    var infos = responseTimeDistributionInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeDistribution");
        return;
    }
    if (isGraph($("#flotResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var syntheticResponseTimeDistributionInfos = {
        data: {"result": {"minY": 31.0, "minX": 0.0, "ticks": [[0, "Requests having \nresponse time <= 500ms"], [1, "Requests having \nresponse time > 500ms and <= 1,500ms"], [2, "Requests having \nresponse time > 1,500ms"], [3, "Requests in error"]], "maxY": 16340.0, "series": [{"data": [[0.0, 16340.0]], "color": "#9ACD32", "isOverall": false, "label": "Requests having \nresponse time <= 500ms", "isController": false}, {"data": [[1.0, 1568.0]], "color": "yellow", "isOverall": false, "label": "Requests having \nresponse time > 500ms and <= 1,500ms", "isController": false}, {"data": [[2.0, 31.0]], "color": "orange", "isOverall": false, "label": "Requests having \nresponse time > 1,500ms", "isController": false}, {"data": [], "color": "#FF6347", "isOverall": false, "label": "Requests in error", "isController": false}], "supportsControllersDiscrimination": false, "maxX": 2.0, "title": "Synthetic Response Times Distribution"}},
        getOptions: function() {
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendSyntheticResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times ranges",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                    tickLength:0,
                    min:-0.5,
                    max:3.5
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    align: "center",
                    barWidth: 0.25,
                    fill:.75
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " " + label;
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            options.xaxis.ticks = data.result.ticks;
            $.plot($("#flotSyntheticResponseTimeDistribution"), prepareData(data.result.series, $("#choicesSyntheticResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshSyntheticResponseTimeDistribution() {
    var infos = syntheticResponseTimeDistributionInfos;
    prepareSeries(infos.data, true);
    if (isGraph($("#flotSyntheticResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerSyntheticResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var activeThreadsOverTimeInfos = {
        data: {"result": {"minY": 11.701117318435744, "minX": 1.5946668E12, "maxY": 16.0, "series": [{"data": [[1.59466686E12, 16.0], [1.59466716E12, 15.524000000000003], [1.59466698E12, 16.0], [1.5946668E12, 11.701117318435744], [1.5946671E12, 16.0], [1.59466692E12, 16.0], [1.59466704E12, 16.0]], "isOverall": false, "label": "bzm - Concurrency Thread Group-ThreadStarter", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.59466716E12, "title": "Active Threads Over Time"}},
        getOptions: function() {
            return {
                series: {
                    stack: true,
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 6,
                    show: true,
                    container: '#legendActiveThreadsOverTime'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                selection: {
                    mode: 'xy'
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : At %x there were %y active threads"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesActiveThreadsOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotActiveThreadsOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewActiveThreadsOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Active Threads Over Time
function refreshActiveThreadsOverTime(fixTimestamps) {
    var infos = activeThreadsOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 0);
    }
    if(isGraph($("#flotActiveThreadsOverTime"))) {
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesActiveThreadsOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotActiveThreadsOverTime", "#overviewActiveThreadsOverTime");
        $('#footerActiveThreadsOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var timeVsThreadsInfos = {
        data: {"result": {"minY": 132.0, "minX": 1.0, "maxY": 1385.2857142857142, "series": [{"data": [[8.0, 137.0], [2.0, 321.0], [9.0, 149.0], [10.0, 361.99186991869914], [11.0, 170.0], [3.0, 1385.2857142857142], [12.0, 207.0], [13.0, 307.9302325581395], [14.0, 132.0], [15.0, 211.0], [16.0, 276.45392569026365], [4.0, 341.0], [1.0, 400.0], [5.0, 181.0], [6.0, 881.5909090909091], [7.0, 171.0]], "isOverall": false, "label": "findOndWiseFlightCombinations- Oneway - Non connections", "isController": false}, {"data": [[15.90751992864704, 278.47282457216005]], "isOverall": false, "label": "findOndWiseFlightCombinations- Oneway - Non connections-Aggregated", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 16.0, "title": "Time VS Threads"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: { noColumns: 2,show: true, container: '#legendTimeVsThreads' },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s: At %x.2 active threads, Average response time was %y.2 ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesTimeVsThreads"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotTimesVsThreads"), dataset, options);
            // setup overview
            $.plot($("#overviewTimesVsThreads"), dataset, prepareOverviewOptions(options));
        }
};

// Time vs threads
function refreshTimeVsThreads(){
    var infos = timeVsThreadsInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTimeVsThreads");
        return;
    }
    if(isGraph($("#flotTimesVsThreads"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTimeVsThreads");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTimesVsThreads", "#overviewTimesVsThreads");
        $('#footerTimeVsThreads .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var bytesThroughputOverTimeInfos = {
        data : {"result": {"minY": 13401.666666666666, "minX": 1.5946668E12, "maxY": 235399.95, "series": [{"data": [[1.59466686E12, 172571.68333333332], [1.59466716E12, 13401.666666666666], [1.59466698E12, 194129.36666666667], [1.5946668E12, 19305.95], [1.5946671E12, 194556.33333333334], [1.59466692E12, 179936.53333333333], [1.59466704E12, 175956.36666666667]], "isOverall": false, "label": "Bytes received per second", "isController": false}, {"data": [[1.59466686E12, 207901.45], [1.59466716E12, 15987.5], [1.59466698E12, 235272.05], [1.5946668E12, 22894.1], [1.5946671E12, 235399.95], [1.59466692E12, 217366.05], [1.59466704E12, 212377.95]], "isOverall": false, "label": "Bytes sent per second", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.59466716E12, "title": "Bytes Throughput Over Time"}},
        getOptions : function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity) ,
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Bytes / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendBytesThroughputOverTime'
                },
                selection: {
                    mode: "xy"
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y"
                }
            };
        },
        createGraph : function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesBytesThroughputOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotBytesThroughputOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewBytesThroughputOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Bytes throughput Over Time
function refreshBytesThroughputOverTime(fixTimestamps) {
    var infos = bytesThroughputOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 0);
    }
    if(isGraph($("#flotBytesThroughputOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesBytesThroughputOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotBytesThroughputOverTime", "#overviewBytesThroughputOverTime");
        $('#footerBytesThroughputOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimesOverTimeInfos = {
        data: {"result": {"minY": 257.23999999999995, "minX": 1.5946668E12, "maxY": 374.0307262569833, "series": [{"data": [[1.59466686E12, 295.4930790525993], [1.59466716E12, 257.23999999999995], [1.59466698E12, 260.7814623539011], [1.5946668E12, 374.0307262569833], [1.5946671E12, 260.5791904373818], [1.59466692E12, 282.111503383348], [1.59466704E12, 288.8163203854256]], "isOverall": false, "label": "findOndWiseFlightCombinations- Oneway - Non connections", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.59466716E12, "title": "Response Time Over Time"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average response time was %y ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Times Over Time
function refreshResponseTimeOverTime(fixTimestamps) {
    var infos = responseTimesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 0);
    }
    if(isGraph($("#flotResponseTimesOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesOverTime", "#overviewResponseTimesOverTime");
        $('#footerResponseTimesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var latenciesOverTimeInfos = {
        data: {"result": {"minY": 257.23199999999997, "minX": 1.5946668E12, "maxY": 373.98603351955336, "series": [{"data": [[1.59466686E12, 295.4780067671493], [1.59466716E12, 257.23199999999997], [1.59466698E12, 260.77548246806134], [1.5946668E12, 373.98603351955336], [1.5946671E12, 260.575658788373], [1.59466692E12, 282.1062077081499], [1.59466704E12, 288.81210478771493]], "isOverall": false, "label": "findOndWiseFlightCombinations- Oneway - Non connections", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.59466716E12, "title": "Latencies Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response latencies in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendLatenciesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average latency was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesLatenciesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotLatenciesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewLatenciesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Latencies Over Time
function refreshLatenciesOverTime(fixTimestamps) {
    var infos = latenciesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyLatenciesOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 0);
    }
    if(isGraph($("#flotLatenciesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesLatenciesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotLatenciesOverTime", "#overviewLatenciesOverTime");
        $('#footerLatenciesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var connectTimeOverTimeInfos = {
        data: {"result": {"minY": 9.227999999999998, "minX": 1.5946668E12, "maxY": 15.351955307262592, "series": [{"data": [[1.59466686E12, 10.436788680406023], [1.59466716E12, 9.227999999999998], [1.59466698E12, 9.49198151671649], [1.5946668E12, 15.351955307262592], [1.5946671E12, 9.387394729693039], [1.59466692E12, 9.746101794645451], [1.59466704E12, 13.091839807286958]], "isOverall": false, "label": "findOndWiseFlightCombinations- Oneway - Non connections", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.59466716E12, "title": "Connect Time Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getConnectTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average Connect Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendConnectTimeOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average connect time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesConnectTimeOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotConnectTimeOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewConnectTimeOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Connect Time Over Time
function refreshConnectTimeOverTime(fixTimestamps) {
    var infos = connectTimeOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyConnectTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 0);
    }
    if(isGraph($("#flotConnectTimeOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesConnectTimeOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotConnectTimeOverTime", "#overviewConnectTimeOverTime");
        $('#footerConnectTimeOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var responseTimePercentilesOverTimeInfos = {
        data: {"result": {"minY": 14.0, "minX": 1.5946668E12, "maxY": 5140.0, "series": [{"data": [[1.59466686E12, 1845.0], [1.59466716E12, 818.0], [1.59466698E12, 1435.0], [1.5946668E12, 5140.0], [1.5946671E12, 1568.0], [1.59466692E12, 1530.0], [1.59466704E12, 4359.0]], "isOverall": false, "label": "Max", "isController": false}, {"data": [[1.59466686E12, 17.0], [1.59466716E12, 16.0], [1.59466698E12, 14.0], [1.5946668E12, 22.0], [1.5946671E12, 14.0], [1.59466692E12, 14.0], [1.59466704E12, 14.0]], "isOverall": false, "label": "Min", "isController": false}, {"data": [[1.59466686E12, 537.6000000000004], [1.59466716E12, 417.8], [1.59466698E12, 455.0], [1.5946668E12, 573.7000000000004], [1.5946671E12, 449.0], [1.59466692E12, 498.0], [1.59466704E12, 491.8000000000002]], "isOverall": false, "label": "90th percentile", "isController": false}, {"data": [[1.59466686E12, 1028.44], [1.59466716E12, 755.7700000000007], [1.59466698E12, 795.7999999999993], [1.5946668E12, 2937.780000000019], [1.5946671E12, 869.2599999999989], [1.59466692E12, 973.0], [1.59466704E12, 1098.6999999999962]], "isOverall": false, "label": "99th percentile", "isController": false}, {"data": [[1.59466686E12, 677.3999999999996], [1.59466716E12, 536.3499999999999], [1.59466698E12, 550.0], [1.5946668E12, 788.1], [1.5946671E12, 561.8999999999996], [1.59466692E12, 637.0], [1.59466704E12, 636.0]], "isOverall": false, "label": "95th percentile", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.59466716E12, "title": "Response Time Percentiles Over Time (successful requests only)"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Response Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentilesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Response time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentilesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimePercentilesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimePercentilesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Time Percentiles Over Time
function refreshResponseTimePercentilesOverTime(fixTimestamps) {
    var infos = responseTimePercentilesOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 0);
    }
    if(isGraph($("#flotResponseTimePercentilesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimePercentilesOverTime", "#overviewResponseTimePercentilesOverTime");
        $('#footerResponseTimePercentilesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var responseTimeVsRequestInfos = {
    data: {"result": {"minY": 190.0, "minX": 1.0, "maxY": 3937.0, "series": [{"data": [[2.0, 2187.0], [3.0, 360.5], [5.0, 583.0], [8.0, 423.0], [13.0, 3937.0], [15.0, 654.0], [22.0, 605.5], [25.0, 365.0], [30.0, 358.5], [31.0, 263.0], [33.0, 369.0], [32.0, 333.0], [35.0, 268.0], [36.0, 296.5], [37.0, 328.0], [38.0, 313.5], [39.0, 326.0], [40.0, 319.0], [41.0, 291.0], [43.0, 293.0], [42.0, 299.0], [44.0, 315.0], [45.0, 313.0], [46.0, 288.5], [47.0, 232.0], [48.0, 307.5], [49.0, 292.0], [51.0, 259.0], [50.0, 290.5], [52.0, 259.5], [53.0, 245.5], [54.0, 233.5], [55.0, 231.0], [56.0, 257.5], [57.0, 228.0], [59.0, 219.0], [58.0, 236.0], [60.0, 230.0], [61.0, 217.0], [63.0, 206.0], [62.0, 216.0], [64.0, 211.5], [67.0, 215.0], [66.0, 208.5], [65.0, 208.0], [68.0, 201.0], [70.0, 198.0], [71.0, 195.0], [69.0, 197.5], [74.0, 199.5], [73.0, 193.0], [75.0, 197.0], [72.0, 194.0], [76.0, 197.5], [77.0, 200.0], [78.0, 190.5], [79.0, 191.0], [83.0, 200.0], [81.0, 202.5], [80.0, 196.0], [82.0, 197.0], [86.0, 195.0], [85.0, 190.0], [1.0, 698.0]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 86.0, "title": "Response Time Vs Request"}},
    getOptions: function() {
        return {
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Response Time in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: {
                noColumns: 2,
                show: true,
                container: '#legendResponseTimeVsRequest'
            },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median response time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesResponseTimeVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotResponseTimeVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewResponseTimeVsRequest"), dataset, prepareOverviewOptions(options));

    }
};

// Response Time vs Request
function refreshResponseTimeVsRequest() {
    var infos = responseTimeVsRequestInfos;
    prepareSeries(infos.data);
    if (isGraph($("#flotResponseTimeVsRequest"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeVsRequest");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimeVsRequest", "#overviewResponseTimeVsRequest");
        $('#footerResponseRimeVsRequest .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var latenciesVsRequestInfos = {
    data: {"result": {"minY": 190.0, "minX": 1.0, "maxY": 3937.0, "series": [{"data": [[2.0, 2187.0], [3.0, 360.5], [5.0, 583.0], [8.0, 422.5], [13.0, 3937.0], [15.0, 654.0], [22.0, 605.5], [25.0, 365.0], [30.0, 358.5], [31.0, 263.0], [33.0, 369.0], [32.0, 333.0], [35.0, 268.0], [36.0, 296.5], [37.0, 328.0], [38.0, 313.5], [39.0, 326.0], [40.0, 318.5], [41.0, 291.0], [43.0, 293.0], [42.0, 299.0], [44.0, 315.0], [45.0, 313.0], [46.0, 288.5], [47.0, 232.0], [48.0, 307.5], [49.0, 292.0], [51.0, 259.0], [50.0, 290.5], [52.0, 259.5], [53.0, 245.5], [54.0, 233.5], [55.0, 231.0], [56.0, 257.5], [57.0, 228.0], [59.0, 219.0], [58.0, 236.0], [60.0, 230.0], [61.0, 216.5], [63.0, 206.0], [62.0, 216.0], [64.0, 211.5], [67.0, 215.0], [66.0, 208.5], [65.0, 208.0], [68.0, 201.0], [70.0, 198.0], [71.0, 195.0], [69.0, 197.5], [74.0, 199.5], [73.0, 193.0], [75.0, 197.0], [72.0, 194.0], [76.0, 197.5], [77.0, 200.0], [78.0, 190.5], [79.0, 191.0], [83.0, 200.0], [81.0, 202.5], [80.0, 196.0], [82.0, 197.0], [86.0, 195.0], [85.0, 190.0], [1.0, 698.0]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 86.0, "title": "Latencies Vs Request"}},
    getOptions: function() {
        return{
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Latency in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: { noColumns: 2,show: true, container: '#legendLatencyVsRequest' },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median Latency time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesLatencyVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotLatenciesVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewLatenciesVsRequest"), dataset, prepareOverviewOptions(options));
    }
};

// Latencies vs Request
function refreshLatenciesVsRequest() {
        var infos = latenciesVsRequestInfos;
        prepareSeries(infos.data);
        if(isGraph($("#flotLatenciesVsRequest"))){
            infos.createGraph();
        }else{
            var choiceContainer = $("#choicesLatencyVsRequest");
            createLegend(choiceContainer, infos);
            infos.createGraph();
            setGraphZoomable("#flotLatenciesVsRequest", "#overviewLatenciesVsRequest");
            $('#footerLatenciesVsRequest .legendColorBox > div').each(function(i){
                $(this).clone().prependTo(choiceContainer.find("li").eq(i));
            });
        }
};

var hitsPerSecondInfos = {
        data: {"result": {"minY": 3.9, "minX": 1.5946668E12, "maxY": 61.35, "series": [{"data": [[1.59466686E12, 54.18333333333333], [1.59466716E12, 3.9], [1.59466698E12, 61.31666666666667], [1.5946668E12, 6.233333333333333], [1.5946671E12, 61.35], [1.59466692E12, 56.65], [1.59466704E12, 55.35]], "isOverall": false, "label": "hitsPerSecond", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.59466716E12, "title": "Hits Per Second"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of hits / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendHitsPerSecond"
                },
                selection: {
                    mode : 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y.2 hits/sec"
                }
            };
        },
        createGraph: function createGraph() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesHitsPerSecond"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotHitsPerSecond"), dataset, options);
            // setup overview
            $.plot($("#overviewHitsPerSecond"), dataset, prepareOverviewOptions(options));
        }
};

// Hits per second
function refreshHitsPerSecond(fixTimestamps) {
    var infos = hitsPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 0);
    }
    if (isGraph($("#flotHitsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesHitsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotHitsPerSecond", "#overviewHitsPerSecond");
        $('#footerHitsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var codesPerSecondInfos = {
        data: {"result": {"minY": 4.166666666666667, "minX": 1.5946668E12, "maxY": 61.35, "series": [{"data": [[1.59466686E12, 54.18333333333333], [1.59466716E12, 4.166666666666667], [1.59466698E12, 61.31666666666667], [1.5946668E12, 5.966666666666667], [1.5946671E12, 61.35], [1.59466692E12, 56.65], [1.59466704E12, 55.35]], "isOverall": false, "label": "200", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.59466716E12, "title": "Codes Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendCodesPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "Number of Response Codes %s at %x was %y.2 responses / sec"
                }
            };
        },
    createGraph: function() {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesCodesPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotCodesPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewCodesPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Codes per second
function refreshCodesPerSecond(fixTimestamps) {
    var infos = codesPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 0);
    }
    if(isGraph($("#flotCodesPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesCodesPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotCodesPerSecond", "#overviewCodesPerSecond");
        $('#footerCodesPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var transactionsPerSecondInfos = {
        data: {"result": {"minY": 4.166666666666667, "minX": 1.5946668E12, "maxY": 61.35, "series": [{"data": [[1.59466686E12, 54.18333333333333], [1.59466716E12, 4.166666666666667], [1.59466698E12, 61.31666666666667], [1.5946668E12, 5.966666666666667], [1.5946671E12, 61.35], [1.59466692E12, 56.65], [1.59466704E12, 55.35]], "isOverall": false, "label": "findOndWiseFlightCombinations- Oneway - Non connections-success", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.59466716E12, "title": "Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTransactionsPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                }
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTransactionsPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTransactionsPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewTransactionsPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Transactions per second
function refreshTransactionsPerSecond(fixTimestamps) {
    var infos = transactionsPerSecondInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTransactionsPerSecond");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 0);
    }
    if(isGraph($("#flotTransactionsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTransactionsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTransactionsPerSecond", "#overviewTransactionsPerSecond");
        $('#footerTransactionsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var totalTPSInfos = {
        data: {"result": {"minY": 4.166666666666667, "minX": 1.5946668E12, "maxY": 61.35, "series": [{"data": [[1.59466686E12, 54.18333333333333], [1.59466716E12, 4.166666666666667], [1.59466698E12, 61.31666666666667], [1.5946668E12, 5.966666666666667], [1.5946671E12, 61.35], [1.59466692E12, 56.65], [1.59466704E12, 55.35]], "isOverall": false, "label": "Transaction-success", "isController": false}, {"data": [], "isOverall": false, "label": "Transaction-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.59466716E12, "title": "Total Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTotalTPS"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                },
                colors: ["#9ACD32", "#FF6347"]
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTotalTPS"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTotalTPS"), dataset, options);
        // setup overview
        $.plot($("#overviewTotalTPS"), dataset, prepareOverviewOptions(options));
    }
};

// Total Transactions per second
function refreshTotalTPS(fixTimestamps) {
    var infos = totalTPSInfos;
    // We want to ignore seriesFilter
    prepareSeries(infos.data, false, true);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 0);
    }
    if(isGraph($("#flotTotalTPS"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTotalTPS");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTotalTPS", "#overviewTotalTPS");
        $('#footerTotalTPS .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

// Collapse the graph matching the specified DOM element depending the collapsed
// status
function collapse(elem, collapsed){
    if(collapsed){
        $(elem).parent().find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    } else {
        $(elem).parent().find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        if (elem.id == "bodyBytesThroughputOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshBytesThroughputOverTime(true);
            }
            document.location.href="#bytesThroughputOverTime";
        } else if (elem.id == "bodyLatenciesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesOverTime(true);
            }
            document.location.href="#latenciesOverTime";
        } else if (elem.id == "bodyCustomGraph") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCustomGraph(true);
            }
            document.location.href="#responseCustomGraph";
        } else if (elem.id == "bodyConnectTimeOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshConnectTimeOverTime(true);
            }
            document.location.href="#connectTimeOverTime";
        } else if (elem.id == "bodyResponseTimePercentilesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimePercentilesOverTime(true);
            }
            document.location.href="#responseTimePercentilesOverTime";
        } else if (elem.id == "bodyResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeDistribution();
            }
            document.location.href="#responseTimeDistribution" ;
        } else if (elem.id == "bodySyntheticResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshSyntheticResponseTimeDistribution();
            }
            document.location.href="#syntheticResponseTimeDistribution" ;
        } else if (elem.id == "bodyActiveThreadsOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshActiveThreadsOverTime(true);
            }
            document.location.href="#activeThreadsOverTime";
        } else if (elem.id == "bodyTimeVsThreads") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTimeVsThreads();
            }
            document.location.href="#timeVsThreads" ;
        } else if (elem.id == "bodyCodesPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCodesPerSecond(true);
            }
            document.location.href="#codesPerSecond";
        } else if (elem.id == "bodyTransactionsPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTransactionsPerSecond(true);
            }
            document.location.href="#transactionsPerSecond";
        } else if (elem.id == "bodyTotalTPS") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTotalTPS(true);
            }
            document.location.href="#totalTPS";
        } else if (elem.id == "bodyResponseTimeVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeVsRequest();
            }
            document.location.href="#responseTimeVsRequest";
        } else if (elem.id == "bodyLatenciesVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesVsRequest();
            }
            document.location.href="#latencyVsRequest";
        }
    }
}

/*
 * Activates or deactivates all series of the specified graph (represented by id parameter)
 * depending on checked argument.
 */
function toggleAll(id, checked){
    var placeholder = document.getElementById(id);

    var cases = $(placeholder).find(':checkbox');
    cases.prop('checked', checked);
    $(cases).parent().children().children().toggleClass("legend-disabled", !checked);

    var choiceContainer;
    if ( id == "choicesBytesThroughputOverTime"){
        choiceContainer = $("#choicesBytesThroughputOverTime");
        refreshBytesThroughputOverTime(false);
    } else if(id == "choicesResponseTimesOverTime"){
        choiceContainer = $("#choicesResponseTimesOverTime");
        refreshResponseTimeOverTime(false);
    }else if(id == "choicesResponseCustomGraph"){
        choiceContainer = $("#choicesResponseCustomGraph");
        refreshCustomGraph(false);
    } else if ( id == "choicesLatenciesOverTime"){
        choiceContainer = $("#choicesLatenciesOverTime");
        refreshLatenciesOverTime(false);
    } else if ( id == "choicesConnectTimeOverTime"){
        choiceContainer = $("#choicesConnectTimeOverTime");
        refreshConnectTimeOverTime(false);
    } else if ( id == "choicesResponseTimePercentilesOverTime"){
        choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        refreshResponseTimePercentilesOverTime(false);
    } else if ( id == "choicesResponseTimePercentiles"){
        choiceContainer = $("#choicesResponseTimePercentiles");
        refreshResponseTimePercentiles();
    } else if(id == "choicesActiveThreadsOverTime"){
        choiceContainer = $("#choicesActiveThreadsOverTime");
        refreshActiveThreadsOverTime(false);
    } else if ( id == "choicesTimeVsThreads"){
        choiceContainer = $("#choicesTimeVsThreads");
        refreshTimeVsThreads();
    } else if ( id == "choicesSyntheticResponseTimeDistribution"){
        choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        refreshSyntheticResponseTimeDistribution();
    } else if ( id == "choicesResponseTimeDistribution"){
        choiceContainer = $("#choicesResponseTimeDistribution");
        refreshResponseTimeDistribution();
    } else if ( id == "choicesHitsPerSecond"){
        choiceContainer = $("#choicesHitsPerSecond");
        refreshHitsPerSecond(false);
    } else if(id == "choicesCodesPerSecond"){
        choiceContainer = $("#choicesCodesPerSecond");
        refreshCodesPerSecond(false);
    } else if ( id == "choicesTransactionsPerSecond"){
        choiceContainer = $("#choicesTransactionsPerSecond");
        refreshTransactionsPerSecond(false);
    } else if ( id == "choicesTotalTPS"){
        choiceContainer = $("#choicesTotalTPS");
        refreshTotalTPS(false);
    } else if ( id == "choicesResponseTimeVsRequest"){
        choiceContainer = $("#choicesResponseTimeVsRequest");
        refreshResponseTimeVsRequest();
    } else if ( id == "choicesLatencyVsRequest"){
        choiceContainer = $("#choicesLatencyVsRequest");
        refreshLatenciesVsRequest();
    }
    var color = checked ? "black" : "#818181";
    if(choiceContainer != null) {
        choiceContainer.find("label").each(function(){
            this.style.color = color;
        });
    }
}

